import React from 'react';
import { connect } from 'react-redux'
import { deletePost } from "../api";
import { Link } from 'react-router-dom';

export const PostDetail = props => {
  const { list, detail } = props

  const handleBack = () => {
    props.history.push('/')
  }

  const handleDelete = () => {
    deletePost(detail.id)
      .then(handleBack)
  }

  return !detail ? null : (
    <div className='page detail'>
      <aside>
        <ul>
          {
            list.map(({ id, title }) => (
              <li key={id}>
                <Link to={`/notes/${id}`}>{title}</Link>
              </li>
            ))
          }
        </ul>
      </aside>
      <section>
        <h1 className='title'>{detail.title}</h1>
        <div className='hr'></div>
        <p>{detail.description}</p>
        <div className='hr'></div>
        <div className='actions'>
          <button onClick={handleDelete} className='primary'>删除</button>
          <button onClick={handleBack}>返回</button>
        </div>
      </section>
    </div>
  );
};

export const PostDetailWithConnect = connect((state, props) => ({
  detail: state.posts.list
    .find(({ id }) => id == props.match.params.id),
  list: state.posts.list
}))(PostDetail)
