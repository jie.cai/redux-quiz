import React, { useState } from 'react';
import { createPost } from "../api";

export const CreatePost = (props) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const form = {
    title,
    description
  }
  const valid = title && description

  const handleTitleChange = event => {
    setTitle(event.target.value.trim())
  }
  const handleDescriptionChange = event => {
    setDescription(event.target.value.trim())
  }

  const handleSubmit = event => {
    event.preventDefault()    
    createPost(form).then(() => {
      props.history.push('/')
    }).catch(() => {
      alert('创建失败，请重试')
    })
  }

  const handleCancel = () => {
    props.history.push('/')
  }

  return (
    <div className='page create'>
      <form onSubmit={handleSubmit}>
        <h2>创建笔记</h2>
        <div className='hr'></div>
        <div className='form-field-item'>
          <label>标题</label>
          <input value={title} onChange={handleTitleChange} />
        </div>
        <div className='form-field-item'>
          <label>正文</label>
          <textarea rows={12} value={description} onChange={handleDescriptionChange}></textarea>
        </div>
        <div className='form-actions'>
          <input disabled={!valid} type='submit' value='提交' className='primary' />
          <button type='button' onClick={handleCancel}>取消</button>
        </div>
      </form>
    </div>
  );
};
