import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { Link } from 'react-router-dom';
import { getPosts } from "../api";
import { setPostList } from "../actions/postActions";

export const Home = props => {
  const { list, setList } = props;

  useEffect(() => {
    getPosts()
      .then(setList)
  }, [])

  return (
    <div className='page home'>
      <ul className='post-cards'>
        {
          list.map(({ id, title }) => (
            <li key={id} className='post-card'>
              <Link to={`/notes/${ id }`}>
                <div className='post-card-inner'>
                  <span>{title}</span>
                </div>
              </Link>
            </li>
          ))
        }
        <li className='post-card'>
          <Link to='/notes/create'>
            <div className='post-card-inner'>
            <span>+</span>
            </div>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export const HomeWithConnect = connect(state => ({
  list: state.posts.list
}), dispatch => bindActionCreators({
  setList: setPostList
}, dispatch))(Home)
