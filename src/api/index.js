export const createPost = post =>
  fetch('http://localhost:8080/api/posts', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(post)
  })

export const getPosts = () =>
  fetch('http://localhost:8080/api/posts')
    .then(response => response.json())

export const deletePost = id =>
  fetch(`http://localhost:8080/api/posts/${id}`, {
    method: 'DELETE',
  })
