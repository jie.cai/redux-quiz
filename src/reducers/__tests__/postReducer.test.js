import postReducer from '../postReducer';

it('should set posts list', () => {
  expect(postReducer({
    list: []
  }, {
    type: 'SET_POST_LIST',
    payload: [
      { title: 'post1' },
      { title: 'post2' }
    ]
  }).list).toHaveLength(2)
})
