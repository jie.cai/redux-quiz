const initState = {
  list: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'SET_POST_LIST':
      return {
        ...state,
        list: action.payload
      };
    default:
      return state
  }
};
