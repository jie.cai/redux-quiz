import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { HomeWithConnect } from './pages/Home';
import { CreatePost } from './pages/CreatePost';
import { PostDetailWithConnect } from './pages/PostDetail';
import { NavHeader } from './components/NavHeader';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { getPosts } from "./api";
import { setPostList } from "./actions/postActions";

const AppPure = (props) => {

  useEffect(() => {
    getPosts()
      .then(props.setList)
  }, [])

  return (
    <Router>
      <NavHeader />
      <Switch>
        <Route path='/' exact component={HomeWithConnect} />
        <Route path='/notes/create' component={CreatePost} />
        <Route path='/notes/:id' component={PostDetailWithConnect} />
      </Switch>
    </Router>
  );
}

export default connect(() => ({}), dispatch => bindActionCreators({
  setList: setPostList
}, dispatch))(AppPure)
