import React from 'react';
import ReactDOM from 'react-dom';
import 'modern-normalize'
import App from './App';
import {Provider} from "react-redux";
import store from "./store";
import './styles/index.less'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
