import React from 'react';
export const NavHeader = () => {
  return (
    <div className='nav-header'>
      <h1>Notes</h1>
    </div>
  );
};
