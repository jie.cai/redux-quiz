export const setPostList = list => (dispatch) => {
  dispatch({
    type: 'SET_POST_LIST',
    payload: list
  });
};
