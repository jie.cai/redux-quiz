import { setPostList } from '../postActions';

describe('setPostList', () => {
  it('should dispatch SET_POST_LIST', () => {
    const dispatch = jest.fn()

    setPostList([
      { title: 'post1' },
      { title: 'post2' }
    ])(dispatch)

    expect(dispatch).toHaveBeenCalledWith({
      type: 'SET_POST_LIST',
      payload: [
        { title: 'post1' },
        { title: 'post2' }
      ]
    })
  });
});
